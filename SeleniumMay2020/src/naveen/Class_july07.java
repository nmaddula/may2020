package naveen;

public class Class_july07 {

	public void method_02() throws Exception {
		System.out.println("Printing the new exception method");
		throw new Exception("Exception occurred:");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		THROWS && THROW
		
//		THROWABLE in place of EXCEPTION we can check the errors in the java language
		
		
		Class_july07 test= new Class_july07();
		try {
			test.method_02();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
