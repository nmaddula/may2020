package naveen;

public class Classs_24 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		ExceptionHandling(Arithmetic)
		/*
		 * try {
		 * 
		 * 
		 * int a = 10; int b = 0; int c = a/b; System.out.println(c); } catch(Exception
		 * e){ e.printStackTrace(); // System.out.println("Failed"); }
		 * System.out.println("End");
		 */
		
		// String Index out of bound Exception
		/*
		 * try { String array = "naveen"; char outOfIndex = array.charAt(7);
		 * 
		 * System.out.println(outOfIndex); } catch(StringIndexOutOfBoundsException e){
		 * e.printStackTrace(); System.out.println("Failed"); }
		 * System.out.println("End");
		 */
		// Both Arithmetic & Index out of bound
		try
		{
			int a = 10; 
			int b = 0; 
			int c = a/b;
			System.out.println(c);
		String array = "naveen";
		char outOfIndex = array.charAt(7);
		
		System.out.println(outOfIndex);
		}
		catch(StringIndexOutOfBoundsException e){
			e.printStackTrace();
			System.out.println("Failed characters");
		}
		catch(ArithmeticException e){
			e.printStackTrace();
			System.out.println("Failed for arithmetic");
		}
		System.out.println("End");
	}

}
