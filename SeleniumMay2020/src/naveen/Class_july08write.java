package naveen;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Class_july08write {

	public static void main(String[] args) throws Throwable {
		// TODO Auto-generated method stub

		File f = new File("src/Naveen/testData.txt");
		
		FileWriter fw = new FileWriter(f);
		
		BufferedWriter bw = new BufferedWriter(fw);
		
		bw.write("Name = Naveen\n");
		
		bw.write("Testing = Selenium");
		
		bw.close();
	}

}
