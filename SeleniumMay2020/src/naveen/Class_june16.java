package naveen;

import java.util.ArrayList;
import java.util.Collections;

public class Class_june16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		ARRAYLIST in DESCENDING ORDER AND ASCENDING ORDER
		ArrayList<Integer> names = new ArrayList<Integer>();
		
		names.add(12);
		names.add(10);
		names.add(15);
		
		Collections.sort(names, Collections.reverseOrder());//Descedning order
//		Collections.sort(names);       // Ascending order
		for(Integer i: names) 
		{
			System.out.println(i);	
		}
		
		
	}

}
