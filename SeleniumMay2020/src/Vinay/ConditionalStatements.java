package Vinay;

public class ConditionalStatements {

	public static void main(String[] args) {

		/*
		 * int sample = 1;
		 * 
		 * 
		 * System.out.println(sample + 1); System.out.println(sample + 2);
		 * System.out.println(sample + 3); System.out.println(sample + 4);
		 * System.out.println(sample + 5);
		 */

		// Conditional Statements

		// Program on Student's marks calculation
		/*
		 * 1. 0-49 -- Fail 2. 50 - 100 -- Pass 3. > 100 -- Invalid 4. < 0 -- Invalid 5.
		 * NULL
		 */

		// Using IF Else
		int studentMarks = null;

		if (studentMarks >= 0 && studentMarks < 50) {
			System.out.println("FAIL");
		} else if (studentMarks >= 50 && studentMarks <= 100) {
			System.out.println("PASS");
		} else if (studentMarks < 0) {
			System.out.println("INVALID MARKS");
		} else if (studentMarks > 100) {
			System.out.println("INVALID MARKS");
		}

		// Using Switch Case
		switch (studentMarks) {

		case 1:
			System.out.println("FAIL");
			break;
		case 50:
			System.out.println("PASS");
			break;

		}

	}

}
