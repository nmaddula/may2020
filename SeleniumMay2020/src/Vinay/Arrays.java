package Vinay;

public class Arrays {

	public static void main(String[] args) {

		/*
		 * String[] allNames = {"sashank","naveen","navya"};
		 * 
		 * int totalItemsInArray = allNames.length;
		 * 
		 * for(int i = 1; i <= totalItemsInArray; i++) {
		 * 
		 * System.out.println(allNames[i-1]); }
		 * 
		 * for(int i = 0; i < totalItemsInArray; i++) {
		 * 
		 * System.out.println(allNames[i]); }
		 */

		String name = "Test User";
		String[] allCharacters = name.split("");
		int lengthOfTheString = allCharacters.length;
		for (int i = 0; i < lengthOfTheString; i++) {
			String individualCharacter = allCharacters[i];
			int counter = 0;
			for (int j = 0; j < lengthOfTheString; j++) {
				if (individualCharacter.equalsIgnoreCase(allCharacters[j])) {
					counter = counter + 1;
				}
			}
			if ((counter % 2) == 0) {
				System.out.println(individualCharacter + " - " + counter);
			}
		}

	}

}
