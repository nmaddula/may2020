package Vinay;

import java.util.LinkedHashMap;

public class Collections_HashMap {

	public static void main(String[] args) {
		
//		HashMap<Integer, String> StudentDetails = new HashMap<Integer, String>();

		LinkedHashMap<Integer, String> StudentDetails = new LinkedHashMap<Integer, String>();
		
		StudentDetails.put(2, "Anusha");
		StudentDetails.put(3, "Navya");
		StudentDetails.put(1, "Naveen");
		StudentDetails.put(4, "Greeshma");
		StudentDetails.put(5, "Nikhil");
		StudentDetails.put(6, "Greeshma");
		StudentDetails.put(5, "Shashank");
		
		
		int totalElements = StudentDetails.size();
		
		System.out.println("Total number of elements in the map:: " + totalElements);
		
		
		for(Integer studentNumber: StudentDetails.keySet()) {
			
//			System.out.println(studentNumber);
			
			System.out.println(StudentDetails.get(studentNumber));
			
			
		}
		

	}

}
