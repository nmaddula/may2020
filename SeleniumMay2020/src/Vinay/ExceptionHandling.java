package Vinay;

public class ExceptionHandling {

	public static void main(String[] args) {

		/*try {
			int a = 10;

			int b = 0;

			int c = a / b;

			System.out.println(c);
		} catch (Exception Vinay) {
			System.out.println("Operation failed");
			Vinay.printStackTrace();
		}

		System.out.println("End of Program");*/
		
		
		/*try {
			String name = "naveen";
			char actualCharacter = name.charAt(3);
			System.out.println(actualCharacter);
			System.out.println("Test line");
		} catch (Exception t) {
			System.out.println("Operation failed");
			t.printStackTrace();
		}

		System.out.println("End of Program");*/
		
		
		
		try {
			int a = 10, b = 10;
			System.out.println(a/b);
			String name = "naveen";
			char actualCharacter = name.charAt(7);
			System.out.println(actualCharacter);
			System.out.println("Test line");
		} catch (ArithmeticException t) {
			System.out.println("Printing from Arithmetic block");
			t.printStackTrace();
		} catch (StringIndexOutOfBoundsException t) {
			System.out.println("Printing from String Index block");
			t.printStackTrace();
		}

		System.out.println("End of Program");
		

	}

}
