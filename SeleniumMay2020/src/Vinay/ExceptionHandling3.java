package Vinay;

public class ExceptionHandling3 {

	public void Login() throws Exception {

		System.out.println("Printing from Exception method");

		throw new Exception("Exception occurred: Please put the data in right format");

	}

	public void Loans() {

		try {
			Login();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("End of Loans execution");

	}

	public static void main(String[] args) {

		ExceptionHandling3 obj = new ExceptionHandling3();

		obj.Loans();

		System.out.println("End of testing");

	}

}
