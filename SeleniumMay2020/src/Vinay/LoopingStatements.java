package Vinay;

public class LoopingStatements {

	public static void main(String[] args) {

		// For Loop

		int a;

		/*
		 * for (a = 1; a <= 100; ) {
		 * 
		 * System.out.println(a);
		 * 
		 * a++; }
		 * 
		 * System.out.println(a);
		 */

		// While Loop

		/*
		 * a = 1; while(a <=100) { System.out.println(a); a++; }
		 */

		// How to stop the Loop

		for (a = 1; a <= 100; a++) {
			if (a == 51) {
				break;
			}
			System.out.println(a);
		}
		
		// Break will terminate the immediate surrounding loop - For or While
		for() {
			for() {
				for() {
					break;
				}
			}
		}
		
		

	}

}
