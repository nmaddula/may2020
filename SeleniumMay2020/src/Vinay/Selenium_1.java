package Vinay;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Selenium_1 {
	
	public WebDriver InitiateDriver() {
		System.setProperty("webdriver.chrome.driver", "E:\\chromedriver\\chromedriver.exe");
		
		return new ChromeDriver();
	}
	
	public void launchUrl(WebDriver driver) {
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.facebook.com/");
		driver.manage().window().maximize();
	}
	
	public void EnterDetailsOnFacebook(WebDriver myDriver, WebDriverWait wait) {
		WebElement FirstNameTextBox = myDriver.findElement(By.xpath("//input[@id='u_0_m']"));

		WebElement SurnameTextBox = myDriver.findElement(By.xpath("//input[@id='u_0_o']"));

		WebElement MobileTextBox = myDriver.findElement(By.xpath("//input[@id='u_0_r']"));

		WebElement PasswordTextBox = myDriver.findElement(By.xpath("//input[@id='password_step_input']"));

		WebElement DayDropDown = myDriver.findElement(By.xpath("//select[@id='day']"));

		WebElement MonthDropDown = myDriver.findElement(By.xpath("//select[@id='month']"));

		WebElement YearDropDown = myDriver.findElement(By.xpath("//select[@id='year']"));

		WebElement SignUpButton = myDriver.findElement(By.xpath("//button[text()='Sign Up']"));

		wait.until(ExpectedConditions.visibilityOf(FirstNameTextBox));
		
		FirstNameTextBox.sendKeys("TestUserFN");

		SurnameTextBox.sendKeys("TestUserSN");

		MobileTextBox.sendKeys("123456789");

		PasswordTextBox.sendKeys("TestUserPWD");

		Select dropDownItem = new Select(DayDropDown);

		dropDownItem.selectByValue("2");

		dropDownItem = new Select(MonthDropDown);

		dropDownItem.selectByVisibleText("Apr");

		dropDownItem = new Select(YearDropDown);

		dropDownItem.selectByIndex(30);

		String userCategory = "Female";

		WebElement CategoryRadioButton = myDriver
				.findElement(By.xpath("//label[text()='" + userCategory + "']/preceding-sibling::input"));

		CategoryRadioButton.click();

		userCategory = "Male";

		CategoryRadioButton = myDriver
				.findElement(By.xpath("//label[text()='" + userCategory + "']/preceding-sibling::input"));

		CategoryRadioButton.click();

		userCategory = "Custom";

		CategoryRadioButton = myDriver
				.findElement(By.xpath("//label[text()='" + userCategory + "']/preceding-sibling::input"));

		CategoryRadioButton.click();
		
		boolean elementSelectedState = CategoryRadioButton.isSelected();
		
		System.out.println("elementSelectedState::: " + elementSelectedState);

		SignUpButton.click();
	}
	
	public WebDriverWait myWait(WebDriver myDriver) {
		
		return new WebDriverWait(myDriver, 60);
	}
	
	public static void main(String[] args) throws Throwable {

		Selenium_1 obj = new Selenium_1();
		
		WebDriver myDriver = obj.InitiateDriver();
		
		WebDriverWait wait = obj.myWait(myDriver);
		
		obj.launchUrl(myDriver);
		
		obj.EnterDetailsOnFacebook(myDriver, wait);
		
		
		

		

		

		myDriver.navigate().to("https://www.flipkart.com/");

		String CurrentURL = myDriver.getCurrentUrl();

		System.out.println(CurrentURL);

		String Title = myDriver.getTitle();

		System.out.println(Title);

//		System.out.println(myDriver.getPageSource());

//		System.out.println(myDriver.getWindowHandle());

		WebElement FlipkartCloseButton = myDriver.findElement(By.xpath("//button[@class='_2AkmmA _29YdH8']"));

		FlipkartCloseButton.click();

		WebElement ElectronicsMenuItem = myDriver.findElement(By.xpath("//span[text()='Electronics']"));
//		WebElement PowerBanks = myDriver.findElement(By.xpath("//a[text()='Power Banks']"));
		WebElement FlipkartHomeImage = myDriver.findElement(By.xpath("//img[@alt='Flipkart']"));

		Actions allActions = new Actions(myDriver);

		allActions.moveToElement(ElectronicsMenuItem).build().perform();

		Thread.sleep(3000);

		allActions.keyDown(Keys.CONTROL).click(FlipkartHomeImage).keyUp(Keys.CONTROL).build().perform();

		Set allHandles = myDriver.getWindowHandles();

		String latestWindowHandle = null;

		for (Object s : allHandles) {
			System.out.println(s);
			latestWindowHandle = s.toString();
		}

		myDriver.switchTo().window(latestWindowHandle);

		FlipkartCloseButton = myDriver.findElement(By.xpath("//button[@class='_2AkmmA _29YdH8']"));
		FlipkartCloseButton.click();

		myDriver.findElement(By.xpath("//input[@class='LM6RPg']")).sendKeys("Entering text in new tab");

		// Switching to frame inside webpage
//		myDriver.switchTo().frame("");

		// Switching to alerts
//		myDriver.switchTo().alert().accept();
//		myDriver.switchTo().alert().dismiss();

		ElectronicsMenuItem = myDriver.findElement(By.xpath("//span[text()='Electronics']"));
		String actualElementName = ElectronicsMenuItem.getText();

		System.out.println("actualElementName :::: " + actualElementName);

		String attributeClass = ElectronicsMenuItem.getAttribute("class");

		System.out.println("attributeClass :::: " + attributeClass);

		boolean elementPresence = ElectronicsMenuItem.isDisplayed();

		System.out.println("elementPresence::: " + elementPresence);

		boolean elementActiveness = ElectronicsMenuItem.isEnabled();

		System.out.println("elementActiveness::: " + elementActiveness);
		
//		myDriver.close();
		
		myDriver.quit();
		
		
		
		
		

		

	}

}
