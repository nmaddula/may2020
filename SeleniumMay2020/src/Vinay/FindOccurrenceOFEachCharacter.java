package Vinay;

public class FindOccurrenceOFEachCharacter {

	public static void main(String[] args) {

		String name = "Test User";
		String[] allCharacters = name.split("");
		int lengthOfTheString = allCharacters.length;
		for (int i = 0; i < lengthOfTheString; i++) {
			String individualCharacter = allCharacters[i];
			int counter = 0;
			for (int j = 0; j < lengthOfTheString; j++) {
				if (individualCharacter.equalsIgnoreCase(allCharacters[j])) {
					counter = counter + 1;
				}
			}
			if ((counter % 2) == 0) {
				System.out.println(individualCharacter + " - " + counter);
			}
		}

	}

}
