package Hybrid.SkeletonFrameworkNew;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Starter {

	public static void main(String[] args) throws Throwable {

		XSSFWorkbook wb = new XSSFWorkbook(new File("src\\main\\Resources\\TestCase_1.xlsx"));

		XSSFSheet sheet = wb.getSheetAt(0);

		int SNoColumnIndex = -1, ActionColumnIndex = -1, TestDataColumnIndex = -1, LocatorColumnIndex = -1;

		for (int i = 0; i < sheet.getRow(0).getLastCellNum(); i++) {

			XSSFCell headerCell = sheet.getRow(0).getCell(i);

			if (headerCell != null && !headerCell.toString().trim().isEmpty()) {

				String headerCellValue = headerCell.toString().trim();

				if (headerCellValue.equalsIgnoreCase("S.No")) {
					SNoColumnIndex = i;
				} else if (headerCellValue.equalsIgnoreCase("Action")) {
					ActionColumnIndex = i;
				} else if (headerCellValue.equalsIgnoreCase("TestData")) {
					TestDataColumnIndex = i;
				} else if (headerCellValue.equalsIgnoreCase("Locator")) {
					LocatorColumnIndex = i;
				}
			}
		}

		int totalNumberOfRowsAvailable = sheet.getLastRowNum();

		Properties prop = new Properties();

		prop.load(new FileInputStream(new File("src\\main\\java\\Hybrid\\SkeletonFrameworkNew\\Locators.properties")));

		for (int i = 1; i <= totalNumberOfRowsAvailable; i++) {

			XSSFCell SNoCell = sheet.getRow(i).getCell(SNoColumnIndex);
			XSSFCell ActionCell = sheet.getRow(i).getCell(ActionColumnIndex);
			XSSFCell TestDataCell = sheet.getRow(i).getCell(TestDataColumnIndex);
			XSSFCell LocatorCell = sheet.getRow(i).getCell(LocatorColumnIndex);

			String SnoCellValue = null, ActionCellValue = null, TestDataCellValue = null, LocatorCellValue = null;

			if (SNoCell != null && !SNoCell.toString().trim().isEmpty()) {
				SnoCellValue = SNoCell.toString().trim();
			}
			if (ActionCell != null && !ActionCell.toString().trim().isEmpty()) {
				ActionCellValue = ActionCell.toString().trim();
			}
			if (TestDataCell != null && !TestDataCell.toString().trim().isEmpty()) {
				TestDataCellValue = TestDataCell.toString().trim();
			}
			if (LocatorCell != null && !LocatorCell.toString().trim().isEmpty()) {
				LocatorCellValue = prop.getProperty(LocatorCell.toString().trim());
			}

			Keywords actualActionValue = Keywords.valueOf(ActionCellValue);

			ExecuteAction(SnoCellValue, actualActionValue, TestDataCellValue, LocatorCellValue);

		}
	}

	public static void ExecuteAction(String SnoCellValue, Keywords ActionCellValue, String TestDataCellValue,
			String LocatorCellValue) throws Throwable {

		switch (ActionCellValue) {
		case LaunchURL:
			SeleniumLibrary.LaunchURL(TestDataCellValue);
			break;
		case ClickElement:
			SeleniumLibrary.ClickElement(LocatorCellValue);
			break;
		case TypeInput:
			SeleniumLibrary.TypeInput(LocatorCellValue, TestDataCellValue);
			break;
		case SelectFromDropDownByVisibleText:
			SeleniumLibrary.SelectFromDropDownByVisibleText(LocatorCellValue, TestDataCellValue);
			break;
		case ExpectPageTitleToBe:
			SeleniumLibrary.ExpectPageTitleToBe(TestDataCellValue);
			break;
		default:
			throw new Exception("Error :::: No such action found. Please check the Action provided." + ActionCellValue);
		}

	}

}
