package Hybrid.SkeletonFrameworkNew;

public enum Keywords {

	LaunchURL,
	ClickElement,
	TypeInput,
	SelectFromDropDownByVisibleText,
	ExpectPageTitleToBe
}
