package Hybrid.SkeletonFrameworkNew;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumLibrary {

	public static WebDriver driver;
	public static WebDriverWait wait;

	public static void LaunchURL(String data) {

		System.setProperty("webdriver.chrome.driver", "src\\main\\drivers\\chromedriver.exe");

		driver = new ChromeDriver();

		wait = new WebDriverWait(driver, 60);

		driver.get(data);

		driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);

		driver.manage().window().maximize();

	}

	public static WebElement element(String data) throws Throwable {

		int totalAvailableElements;

		for (int i = 1; i <= 60; i++) {

			totalAvailableElements = driver.findElements(By.xpath(data)).size();

			if (totalAvailableElements > 0) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

		return driver.findElement(By.xpath(data));
	}

	public static void ClickElement(String data) throws Throwable {

		WebElement ele = element(data);

		wait.until(ExpectedConditions.elementToBeClickable(ele));

		ele.click();
	}

	public static void TypeInput(String locator, String data) throws Throwable {

		WebElement ele = element(locator);

		wait.until(ExpectedConditions.visibilityOf(ele));

		ele.sendKeys(data);
	}

	public static void SelectFromDropDownByVisibleText(String locator, String data) throws Throwable {

		WebElement ele = element(locator);

		wait.until(ExpectedConditions.elementToBeClickable(ele));

		Select select = new Select(ele);
		select.selectByVisibleText(data);
	}

	public static void ExpectPageTitleToBe(String data) throws Throwable {

		String actualData = driver.getTitle().trim();

		if (!actualData.equals(data)) {
			throw new Exception("Exception :::: Actual title ->" + actualData + "<- and expected title ->" + data
					+ "<- are not matching");
		}

	}

}
