package selenium_Naveen;

import java.util.concurrent.PriorityBlockingQueue;

import org.apache.poi.ddf.EscherColorRef.SysIndexSource;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class TestNG_Demo_Sep11 {

	@Test()
	public void t1() {
		System.out.println("Executing using TestNG 1");
	}
	@Test()
	public void t2() {
		System.out.println("Executing using TestNG 2");
	}
	@AfterTest
	public void after_test() {
		System.out.println("Executing using After Test");
	}
	@AfterMethod
	public void after_method() {
		System.out.println("Executing using After Method");
	}
	@AfterSuite
	public void after_suite() {
		System.out.println("Executing using After Suite");
	}
	@AfterClass
	public void after_class() {
		System.out.println("Executing using After Class");
	}
	@BeforeClass
	public void before_class() {
		System.out.println("Executing using Before_Class");
	}
	@BeforeMethod
	public void before_method() {
		System.out.println("Executing using Before_Method");
	}
	@BeforeSuite
	public void before_suite() {
		System.out.println("Executing using Before_Suite");
	}
	@BeforeTest
	public void before_test() {
		System.out.println("Executing using Before_Test");
	}
}
