package selenium_Naveen;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestNG_Demo_Sep16 {

	@Test(dataProvider = "AllUserCredentials")
	public void SecondMethod(String userName, String password) {
		System.out.println("USername is " + userName);
		System.out.println("Password is " + password);

	}

	@DataProvider(name = "AllUserCredentials")
	public Object[][] ReturningAllCredentials() throws Throwable, Throwable {
		File f = new File("C:\\Softwares\\Book___.xlsx");
		XSSFWorkbook wb = new XSSFWorkbook(f);
		XSSFSheet mySheet = wb.getSheet("Sheet1");
		int lastRownum = mySheet.getLastRowNum();
		XSSFRow myRow;
		Object[][] allUserObject = new Object[lastRownum][2];

		for (int i = 1; i < lastRownum; i++) {
			myRow = mySheet.getRow(i);
			 
				String myCell_User = myRow.getCell(0).toString();

				String myCell_Pwd = myRow.getCell(1).toString();
				
				allUserObject[i][0] = myCell_User;
				allUserObject[i][1] = myCell_Pwd;
		}
		
		


		return allUserObject;
	}

}
