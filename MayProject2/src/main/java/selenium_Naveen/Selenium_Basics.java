package selenium_Naveen;

import java.beans.Visibility;
import java.sql.Driver;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Selenium_Basics {

	public String pricingMenuXpath = "(//a[contains(@class,'nav-links')][text()='Pricing'])[1]";

	public String signUpXpath = "//a[@class=\"button\"]";

	public String mobileNumberXpath = "//input[@name=\"mobile\"]";
	
	public String url = "https://zerodha.com/";

	public String webURL = "https://www.facebook.com/";

	public String createAccount = "//a[@id=\"u_0_2\"]";

	public String monthDropDownItem = "(//select[@id=\"month\"])";

	public String DayDropDownItem = "(//select[@id=\"day\"])";

	public String YearDropDownItem = "(//select[@id=\"year\"])";

	public String FlipkartuRL = "https://www.flipkart.com/";
	
	public String FlipkartClose = "//button[@class=\"_2AkmmA _29YdH8\"]";
	
	public String Home_Furniture = "//span[text()='Home & Furniture']";
	
	public String FlipkartHomePage = "//img[@title=\"Flipkart\"]";

	public String FlipkartSearch = "//input[@class=\"LM6RPg\"]";
	
	public void clickButton(WebDriver driver, String elementXpath) {
		WebElement ele = WebElement(driver, elementXpath);
		if (ele.isEnabled()) {
			System.out.println("Element is enabled");
			ele.click();
		}
	}

	public void sendKeysbutton(WebDriver driver, String elementXpath, String keysTosend) throws Throwable {
		WebElement ele = WebElement(driver, elementXpath);
		if (ele.isEnabled()) {
			System.out.println("Element is enabled");
			Thread.sleep(2000);
			ele.sendKeys(keysTosend);
		}
	}

	public void DropDown(WebDriver driver, String elementXpath, String textto) {
		WebElement ele = WebElement(driver, elementXpath);
		Select dropItem = new Select(ele);
		dropItem.selectByVisibleText(textto);
	}

	public void naviGate(WebDriver driver, String uRL) {
		driver.navigate().to(uRL);
	}

	public WebDriver InitiateWebDriver() {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Softwares\\Jars\\chromedriver.exe");
		return new ChromeDriver();
		
	}
	
	public void LaunchURL(WebDriver driver,String url) {
	
		driver.get(url);
	}
	
	public Object ActionsonSelenium(WebDriver Driver) {
		return new Actions(Driver);
	}
	
	public void MouseOver(WebDriver Driver,String elementXpath) {
		
		Actions allActions = (Actions) ActionsonSelenium(Driver);
		
		WebElement ele = WebElement(Driver, elementXpath);
		
		allActions.moveToElement(ele).build().perform();
		
	}
	
	public void ControlClick(WebDriver Driver, String elementXpath) {
		Actions allActions = (Actions) ActionsonSelenium(Driver);
		WebElement ele = WebElement(Driver, elementXpath);
		allActions.keyDown(Keys.CONTROL).click(ele).keyUp(Keys.CONTROL).build().perform();
	}
	public WebElement WebElement(WebDriver driver, String locator) {

		WebDriverWait wait = new WebDriverWait(driver, 30);
		return wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
	}

	public static void main(String[] args) throws Throwable {
		Selenium_Basics obj = new Selenium_Basics();
		WebDriver driver = obj.InitiateWebDriver();
		obj.LaunchURL(driver, "https://zerodha.com/");
		driver.manage().window().maximize();
		obj.clickButton(driver, obj.pricingMenuXpath);
		Thread.sleep(2000);
		obj.clickButton(driver, obj.signUpXpath);
		obj.sendKeysbutton(driver, obj.mobileNumberXpath, "2263321896");
		obj.naviGate(driver, obj.webURL);
		obj.clickButton(driver, obj.createAccount);
		obj.DropDown(driver, obj.monthDropDownItem, "Mar");
		obj.DropDown(driver, obj.DayDropDownItem, "2");
		obj.DropDown(driver, obj.YearDropDownItem, "1997");
		obj.naviGate(driver, obj.FlipkartuRL);
		obj.clickButton(driver, obj.FlipkartClose);
		obj.MouseOver(driver, obj.Home_Furniture);
		obj.ControlClick(driver, obj.FlipkartHomePage);
		Set<String> windowIds = driver.getWindowHandles();
		String latestWindowHandle = null;
		for(String s:windowIds ) {
			System.out.println(s);
			latestWindowHandle = s;
		}
		obj.sendKeysbutton(driver, obj.FlipkartSearch, "Shoes");
		driver.switchTo().window(latestWindowHandle);
		obj.clickButton(driver, obj.FlipkartClose);
		obj.sendKeysbutton(driver, obj.FlipkartSearch, "Trousers");
		driver.close();
//		driver.quit();
	}

}
