package selenium_Naveen;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Class_excel {

	public static void main(String[] args) throws Throwable, Throwable {

		File f = new File("C:\\Softwares\\Book__1.xlsx");
		XSSFWorkbook wb = new XSSFWorkbook(f);
		XSSFSheet mySheet = wb.getSheet("Sheet1");
		XSSFRow headingRow = mySheet.getRow(0);
		
		int Sno = -1;
		int actionnumber = -1;
		int testdatanumber = -1;
		int locatornumber = -1;
		
		
		for(int i = 0; i < headingRow.getLastCellNum(); i++)	
		{
			XSSFCell cell_ = headingRow.getCell(i);
			String headingCellValue = null;
			if(cell_ != null)
			{
				headingCellValue = cell_.toString();
			}
			if(headingCellValue.equalsIgnoreCase("S. No"))
			{
				Sno = i;
	
			}
			else if(headingCellValue.equalsIgnoreCase("Actions"))
			{
				actionnumber = i;
	
			}
			else if(headingCellValue.equalsIgnoreCase("TestData"))
			{
				testdatanumber = i;
	
			}
			else if(headingCellValue.equalsIgnoreCase("Locator"))
			{
				locatornumber = i;
	
			}
		}
		
		for(int i=0;i<=mySheet.getLastRowNum();i++)
		{
			XSSFRow myRow = mySheet.getRow(i);
			if(myRow != null)
			{
				XSSFCell SnoCell = myRow.getCell(Sno);
				XSSFCell ActionsCell = myRow.getCell(actionnumber);
				XSSFCell testDataCell = myRow.getCell(testdatanumber);
				XSSFCell locatorCell = myRow.getCell(locatornumber);
				
				if(SnoCell != null)
				{
					System.out.print(SnoCell.toString() + "");
				}
				if(ActionsCell != null)
				{
					System.out.print(ActionsCell.toString() + "");
				}
				if(testDataCell != null)
				{
					System.out.print(testDataCell.toString()+"");
					
				}
				if(locatorCell != null)
				{
					System.out.print(locatorCell.toString() + "");
				}
				
				
			}
			System.out.println();
			
		}
		

	}
}
