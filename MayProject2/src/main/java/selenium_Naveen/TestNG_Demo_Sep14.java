package selenium_Naveen;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestNG_Demo_Sep14 {

//	@Test
//	@Parameters({"userName","pwd"})
//	public void InitialMethod(String userName,String password) {
//		System.out.println("USername is " + userName);
//		System.out.println("Password is " + password);
//
//	}
	@Test(dataProvider = "AllUserCredentials")
	public void SecondMethod(String userName,String password) {
		System.out.println("USername is " + userName);
		System.out.println("Password is " + password);

	}
	@DataProvider(name = "AllUserCredentials")
	public Object[][] ReturningAllCredentials(){
		Object[][] allUserObject = new Object[2][2];
		allUserObject[0][0] = "Naveen";
		allUserObject[0][1] = "Naveenpwd";
		allUserObject[1][0] = "Greeshma";
		allUserObject[1][1] = "Greeshmapwd";
		return allUserObject;
	}

	
}
