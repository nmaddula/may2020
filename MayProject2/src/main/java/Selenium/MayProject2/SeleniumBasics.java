package Selenium.MayProject2;

import java.awt.Desktop.Action;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumBasics {

	public String PricingMenuOptionXpath = "(//a[contains(@class,'nav-links')][text()='Pricing'])[1]";
	public String SignupNowButtonXpath = "//a[text()='Sign up now']";
	public String PhoneNumberXpath = "//input[@id=\"user_mobile\"]";
	public String CreateNewAccountbutton = "//a[text()='Create New Account']";
	public String birthDayDropDown = "//select[@name='birthday_day']";
	public String FlipkartCloseButton = "//button[@class='_2AkmmA _29YdH8']";
	public String TvMenuXpath = "//span[text()='TVs & Appliances']";
	public String FlipkartHomeIcon = "//img[@title='Flipkart']";
	public String SearchTextBox = "//input[@class='LM6RPg']";
	

	public void ClickElement(WebDriver driver, String elementXpath) throws Throwable {

		WebElement ele = WebElement(driver, elementXpath);

		if (ele.isEnabled()) {
			System.out.println("Element is Enabled");
			Thread.sleep(2000);
			ele.click();
		}
	}

	public void SendInput(WebDriver driver, String elementXpath, String dataToType) throws Throwable {
		WebElement ele = WebElement(driver, elementXpath);
		if (ele.isEnabled()) {
			System.out.println("Element is Enabled");
			Thread.sleep(2000);
			ele.clear();
			ele.sendKeys(dataToType);
		}
	}

	public WebElement WebElement(WebDriver driver, String locator) {

		WebDriverWait wait = new WebDriverWait(driver, 30);

		return wait.until(ExpectedConditions.elementToBeClickable((By.xpath(locator))));
	}

	public WebDriver InitiateWebDriver() {

		System.setProperty("webdriver.chrome.driver", "E:\\chromedriver\\chromedriver.exe");

		return new ChromeDriver();

	}

	public void LaunchBrowserWithUrl(WebDriver driver, String url) {
		driver.get(url);
	}

	public void NavigateToSite(WebDriver driver, String UrlToNavigate) {

		driver.navigate().to(UrlToNavigate);
	}

	public void SelectValueFromDropDown(WebDriver driver, String elementXpath, String textToSelect) {

		WebElement ele = WebElement(driver, elementXpath);

		Select dd = new Select(ele);

		dd.selectByVisibleText(textToSelect);

	}

	public Object allActionsOfSelenium(WebDriver driver) {

		return new Actions(driver);
	}

	public void mouseOverOn(WebDriver driver, String elementXpath) {

		Actions allActions = (Actions) allActionsOfSelenium(driver);

		WebElement ele = WebElement(driver, elementXpath);

		allActions.moveToElement(ele).build().perform();
	}

	public void ClickWithControl(WebDriver driver, String elementXpath) {

		Actions allActions = (Actions) allActionsOfSelenium(driver);

		WebElement ele = WebElement(driver, elementXpath);

		allActions.keyDown(Keys.CONTROL).click(ele).keyUp(Keys.CONTROL).build().perform();
	}

	public static void main(String[] args) throws Throwable {

		SeleniumBasics obj = new SeleniumBasics();

		WebDriver driver = obj.InitiateWebDriver();

		obj.LaunchBrowserWithUrl(driver, "https://zerodha.com/");

		driver.manage().window().maximize();

		obj.ClickElement(driver, obj.PricingMenuOptionXpath);

		obj.ClickElement(driver, obj.SignupNowButtonXpath);

		obj.SendInput(driver, obj.PhoneNumberXpath, "90088");

		obj.SendInput(driver, obj.PhoneNumberXpath, "3434");

		obj.NavigateToSite(driver, "https://www.facebook.com");

		obj.ClickElement(driver, obj.CreateNewAccountbutton);

		obj.SelectValueFromDropDown(driver, obj.birthDayDropDown, "28");

		obj.NavigateToSite(driver, "https://www.flipkart.com");

		obj.ClickElement(driver, obj.FlipkartCloseButton);

		obj.mouseOverOn(driver, obj.TvMenuXpath);
		
		obj.ClickWithControl(driver, obj.FlipkartHomeIcon);
		
		Set<String> allCurrentsWindowIds = driver.getWindowHandles();
		
		String latestWindowHandle = null;
		
		for(String s: allCurrentsWindowIds) {
			
			System.out.println(s);
			latestWindowHandle = s;
		}
		
		obj.SendInput(driver, obj.SearchTextBox, "Shoes");
		
		driver.switchTo().window(latestWindowHandle);
		
		obj.SendInput(driver, obj.SearchTextBox, "Shirts");
		
//		driver.close();
		
		driver.quit();
		
		

	}

}
